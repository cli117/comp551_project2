from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.metrics import accuracy_score, confusion_matrix
from DataProcess import *


def linear_regression(train_x, train_y, test_x, test_y):
    lr_instance = LogisticRegression()
    lr_instance.fit(train_x, train_y)
    result = lr_instance.predict(test_x)
    print("Linear regression Accuracy: ")
    print(accuracy_score(test_y, result))

def produce_predict_csv(modelInstance, X_train, y_train, testData, id_to_cat):
    modelInstance.fit(X_train, y_train)
    result = modelInstance.predict(testData)
    write_file = []
    ids = []
    cats = []
    for i in range(len(result)):
        ids += [i]
        cats += [id_to_cat[result[i]]]
    write_file = list(zip(ids, cats))

    pd.DataFrame(write_file, columns=['id', 'Category']).to_csv('prediction.csv', index=False)

if __name__ == '__main__':
    reddit_train_x, reddit_train_y, reddit_test_x, id_to_cat= get_reddit()
    # models = [
    #     LinearSVC(loss='hinge'),
    #     BernoulliNB(alpha=0.2),
    #     MultinomialNB(),
    #     LogisticRegression(random_state=0, solver='saga', multi_class='auto')
    # ]
    # CV = 5
    # cv_df = pd.DataFrame(index=range(CV * len(models)))
    # entries = []
    # for model in models:
    #     model_name = model.__class__.__name__
    #     print(model_name, " is running!")
    #     accuracies = cross_val_score(model, reddit_train_x, reddit_train_y, scoring='accuracy', cv=CV, n_jobs=5)
    #     for fold_idx, accuracy in enumerate(accuracies):
    #         entries.append((model_name, fold_idx, accuracy))
    # cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])
    # print(cv_df.groupby('model_name').accuracy.mean())

    clf = MultinomialNB()
    produce_predict_csv(clf, reddit_train_x, reddit_train_y, reddit_test_x, id_to_cat)

