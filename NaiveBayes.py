import numpy as np


def predict(w, w_0, test_x):
    classes = 20
    prediction = [0] * len(test_x)
    for i in range(len(test_x)):
        y_predict = [0] * classes
        x = test_x[i]
        for j in range(classes):
            y_predict[j] = np.inner(x, w[j]) + w_0[j]
        prediction[i] = np.argmax(y_predict)
    return prediction


def train(classes, train_x, train_y):
    number_of_data = len(train_x)
    features = len(train_x[0])
    w_0 = [0] * classes
    w = np.zeros((classes, features))
    # We have a number_of_data * features matrix, where features = m

    classification = np.zeros((number_of_data, classes))
    # classify different y classes
    for i in range(number_of_data):
        data_y = train_y[i]
        classification[i][data_y].insert(1)

    for i in range(classes):
        new_design_matrix = np.append(train_x,classification[i])
        new_design_matrix0 = new_design_matrix[new_design_matrix[:,len(new_design_matrix)-1] == 0, :]
        new_design_matrix1 = new_design_matrix[new_design_matrix[:, len(new_design_matrix) - 1] == 1, :]
        w_0[i] = np.log(len(new_design_matrix1/new_design_matrix0))
        for k in range (len(train_x)-1):
            feature_w_j_0_down = new_design_matrix0[new_design_matrix0[:, k] == 0, :] #P(x_j=0|y=0)
            feature_w_j_0_up = new_design_matrix1[new_design_matrix1[:, k] == 0, :] #P(x_j=0|y=1)
            feature_w_j_1_down = new_design_matrix0[new_design_matrix0[:, k] == 1, :]  # P(x_j=1|y=0)
            feature_w_j_1_up = new_design_matrix1[new_design_matrix1[:, k] == 1, :]  # P(x_j=1|y=1)
            w_0[i] = w_0[i] + np.log(len(feature_w_j_0_up)/len(feature_w_j_0_down))
            w[i][k] = np.log(len(feature_w_j_1_up) / len(feature_w_j_1_down))-np.log(len(feature_w_j_0_up)/len(feature_w_j_0_down))

    return w_0, w