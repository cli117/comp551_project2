import numpy as np
import pandas as pd
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import preprocessing

from collections import defaultdict

import matplotlib.pyplot as plt

porter = PorterStemmer()
lemmatizer = WordNetLemmatizer()

def drop_stopwords(input_list):
    stoplist = stopwords.words('english')
    clean = [[word for word in sentence.split(" ") if word not in stoplist] for sentence in input_list]
    i = 0
    for word_list in clean:
        input_list[i] = " ".join(word_list)
        i = i + 1
    return input_list

def stem_list(input_list):
    input_list = [[porter.stem(word) for word in sentence.split(" ")] for sentence in input_list]
    i = 0
    for word_list in input_list:
        input_list[i] = " ".join(word_list)
        i = i + 1

    return input_list

def lemmatize_list(input_list):
    input_list = [[lemmatizer.lemmatize(word) for word in sentence.split(" ")] for sentence in input_list]
    i = 0
    for word_list in input_list:
        input_list[i] = " ".join(word_list)
        i = i + 1
    return input_list

def remove_infrequent(input_list, thresholdAsPersentage):
    # Calculate the frequencies and save them in a dictionary
    frequencies = defaultdict(int)
    for words in [sentence.split(" ") for sentence in input_list]:
        for word in words:
            frequencies[word] += 1

    thresholdAsNum = len(frequencies) * thresholdAsPersentage / 100

    result = []
    tempLine = []
    for words in [sentence.split(" ") for sentence in input_list]:
        for word in words:
            if frequencies[word] > thresholdAsNum:
                tempLine.append(word)
        result.append(tempLine)
        tempLine = []

    i = 0
    for word_list in result:
        result[i] = " ".join(word_list)
        i += 1
    return result

def get_reddit():
    # read file
    reddit_train_text = pd.read_csv("Datasets/reddit_train.csv", sep=',')
    reddit_test_text = pd.read_csv("Datasets/reddit_test.csv", sep=',')['comments']

    # drop 'id' and assign id for each subreddit using factorize()
    col = ['comments', 'subreddits']
    reddit_train_text = reddit_train_text[col]
    reddit_train_text['category_id'] = reddit_train_text['subreddits'].factorize()[0]

    # create 2 dictionary for future
    category_id_reddit = reddit_train_text[['subreddits', 'category_id']].drop_duplicates().sort_values('category_id')
    category_to_id = dict(category_id_reddit.values)
    id_to_category = dict(category_id_reddit[['category_id', 'subreddits']].values)

    for i in range(20):
        print(i, id_to_category[i])
    # draw a figure to check #samples in each subreddit
    # fig = plt.figure(figsize=(8, 6))
    # reddit_train_text.groupby('subreddits').comments.count().plot.bar(ylim=0)
    # plt.show()

    # reddit_train_text['comments'] = remove_infrequent(reddit_train_text['comments'], 0.0001)
    # reddit_test_text = remove_infrequent(reddit_test_text, 0.0001)
    X_train = reddit_train_text['comments']
    y_train = reddit_train_text['category_id']
    count_vect = CountVectorizer(min_df=2, ngram_range=(1, 1), binary=False)
    X_train_counts = count_vect.fit_transform(X_train)
    tfidf_transformer = TfidfTransformer(sublinear_tf=True)
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    X_test = tfidf_transformer.transform(count_vect.transform(reddit_test_text))


    return X_train_tfidf, y_train, X_test, id_to_category
if __name__ == '__main__':
    get_reddit()